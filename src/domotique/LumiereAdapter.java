package domotique;

public class LumiereAdapter implements Composant {

	private Lumiere lum;

	public LumiereAdapter(Lumiere l) {
		this.lum = l;
	}

	public void allumer() {
		if (this.lum.getLumiere() + 10 > 100) {
			this.lum.changerIntensite(100);
		} else {
			this.lum.changerIntensite(this.lum.getLumiere() + 10);
		}
	}

	public void eteindre() {
		this.lum.changerIntensite(0);
	}

	public String toString() {
		return this.lum.toString();
	}
}
