package domotique;

public class Main_AFaire {

	public static void main(String args[]) {
		Telecommande t = new Telecommande();

		Lampe l1 = new Lampe("Lampe1");
		t.ajouterComposant(l1);

		/****** A COMPLETER ******/
		t.ajouterComposant(new LumiereAdapter(new Lumiere()));
		t.ajouterComposant(new Hifi());

		TelecommandeGraphique tg = new TelecommandeGraphique(t);
	}

}
