package domotique;

import java.util.*;

public class Telecommande {

	private ArrayList<Composant> composant;

	public Telecommande() {
		this.composant = new ArrayList<Composant>();
	}

	public ArrayList<Composant> getComposant() {
		return this.composant;
	}
	
	public int getNombre() {
		return composant.size();
	}

	public void ajouterComposant(Composant c) {
		if (c != null) {
			this.composant.add(c);
		}
	}

	public void activerComposant(int indiceComposant) {
		if (indiceComposant < this.composant.size() && indiceComposant >= 0) {
			this.composant.get(indiceComposant).allumer();
		}
	}

	public void desactiverComposant(int indiceComposant) {
		if (indiceComposant < this.composant.size() && indiceComposant >= 0) {
			this.composant.get(indiceComposant).eteindre();
		}
	}

	public void activerTout() {
		for (int i = 0; i < this.composant.size(); i++) {
			this.composant.get(i).allumer();

		}
	}

	public String toString() {
		return composant.toString();
	}
}
