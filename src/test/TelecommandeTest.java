package test;

import static org.junit.Assert.*;

import org.junit.Test;

import domotique.*;

public class TelecommandeTest {

	@Test
	public void test_ajouterComposant_telecommandeVide() {
		Telecommande t = new Telecommande();
		Lampe led = new Lampe("LED");
		t.ajouterComposant(led);
		assertEquals("Liste de taille 1", 1, t.getComposant().size());
	}

	@Test
	public void test_ajouterComposant_telecommandeNonVide() {
		Telecommande t = new Telecommande();
		Lampe led = new Lampe("LED");
		Lampe diode = new Lampe("Diode");
		t.ajouterComposant(led);
		t.ajouterComposant(diode);
		assertEquals("Liste de taille 2", 2, t.getComposant().size());
	}

	@Test
	public void test_allumer_position0() {
		Telecommande t = new Telecommande();
		Lampe led = new Lampe("LED");
		led.allumer();
		t.ajouterComposant(led);
		assertEquals("Lampe led allumee", true, led.isAllume());
	}

	@Test
	public void test_allumer_position1() {
		Telecommande t = new Telecommande();
		Lampe l = new Lampe("l");
		t.ajouterComposant(l);
		Lampe led = new Lampe("LED");
		led.allumer();
		t.ajouterComposant(led);
		assertEquals("Lampe led allumee", true, led.isAllume());
	}

	@Test
	public void test_activerComposant_inexistant() {
		Telecommande t = new Telecommande();
		t.activerComposant(0);
		assertEquals("OK", true, t.getComposant().isEmpty());
	}
	
	@Test
	public void test_lumiereAllumee_OK() {
		Telecommande t = new Telecommande();
		Lumiere l = new Lumiere();
		LumiereAdapter lum = new LumiereAdapter(l);
		t.ajouterComposant(lum);
		t.activerComposant(0);
		t.activerComposant(0);
		assertEquals("OK", l.getLumiere(), 20);
	}
	
	@Test
	public void test_lumiereEteindre_OK() {
		Telecommande t = new Telecommande();
		Lumiere l = new Lumiere();
		LumiereAdapter lum = new LumiereAdapter(l);
		t.ajouterComposant(lum);
		t.activerComposant(0);
		t.activerComposant(0);
		t.desactiverComposant(0);
		assertEquals("OK", l.getLumiere(), 0);
	}
}
